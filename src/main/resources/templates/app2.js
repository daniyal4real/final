var app = angular.module('aitu-project', []);
app.controller('RegisterCtrl', function($scope, $http) {

    $scope.registration = {
        email: '',
        password: ''
    }

    // let register = function (registration) {
    //     $http({
    //         url: 'http://127.0.0.1:9090/api/registration',
    //         method: "POST",
    //         headers: {
    //             "Access-Control-Allow-Origin": "*",
    //             "Content-Type": "application/json",
    //         },
    //         data: {
    //             'email' : registration.email,
    //             'password' : registration.password,
    //         }
    //     })
    //         .then(function (response) {
    //                 $scope.registration = response.data;
    //             },
    //             function (response) { // optional
    //                 $scope.registration = {}
    //             });
    // };

    $scope.registerUser = function (registration) {
        $http({
            url: 'http://127.0.0.1:9090/api/registration',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },
            data: {
                'email' : registration.email,
                'password' : registration.password,
            }
        })
            .then(function (response) {
                    $scope.registration = response.data;
                    register(registration);
                },
                function (response) { // optional
                    $scope.registration = {}
                });
    };

});
