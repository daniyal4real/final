package kz.aitu.project.controller;

import kz.aitu.project.entity.Authorization;
import kz.aitu.project.entity.Registration;
import kz.aitu.project.service.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
public class RegistrationController {

    private RegistrationService registrationService;

    @PostMapping("/api/registration")
    public void register( @RequestBody Registration registration){
        registrationService.create(registration);
    }

}
