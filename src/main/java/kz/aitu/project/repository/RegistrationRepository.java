package kz.aitu.project.repository;


import kz.aitu.project.entity.Registration;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface RegistrationRepository extends CrudRepository<Registration, Long> {
    @Transactional
    @Modifying
    @Query(value="INSERT INTO auth(email, password) VALUES( :email, :password)", nativeQuery = true)
    void createRegistration( @Param("email") String email, @Param("password") String password);

}
