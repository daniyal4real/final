package kz.aitu.project.service;


import kz.aitu.project.entity.Registration;
import kz.aitu.project.repository.RegistrationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class RegistrationService {

    private RegistrationRepository registrationRepository;


    public void create(Registration registration) {
        String email = registration.getEmail();
        String password = registration.getPassword();
        registrationRepository.createRegistration(email, password);
    }
}
